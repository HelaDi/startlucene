import java.io.IOException;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopScoreDocCollector;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.RAMDirectory;

public class StartLucene {
	

	  public static void main(String[] args) throws IOException, ParseException {
	    // 0. Specify the analyzer for tokenizing text.
	    //    The same analyzer should be used for indexing and searching
	    StandardAnalyzer analyzer = new StandardAnalyzer();

	    // 1. create the index
	    Directory index = new RAMDirectory();
	    IndexWriterConfig config = new IndexWriterConfig(analyzer);

	    IndexWriter w = new IndexWriter(index, config);
	    addDoc(w, "Document #1", "information retrieval is the most awesome class I ever took.");
	    addDoc(w, "Document #2", "the retrieval of private information from your emails is a job that the NSA loves.");
	    addDoc(w, "Document #3", "in the school of information you can learn about data science.");
	    addDoc(w, "Document #4", "the labrador retriever is a great dog.");
	    w.close();
	    
	    //2. query
	    String querystr = args.length > 0 ? args[0] : "lucene";
	    
	    //2.1 convert user input to standard query
	    String[] token = querystr.split(" ");
	    int tempIndexAND = 0;
	    int tempIndexNOT = 0;
	    String query = "";
	    
	    if (hasAND(token)){
	    	tempIndexAND = findIndexAND(token);
	    	System.out.println(tempIndexAND);
	    	if (tempIndexAND == 1){
	    		query = query + token[0] + " ";
	    	}else{
	    		query = query + "\"";
	    		for (int i = 0; i < tempIndexAND; i++){
	    			query = query + token[i] + " ";
	    		}
	    		query = query + "\"";
	    	}
	    	query = query + "AND" + " ";
	    	
	    	if(hasNOT(token)){
	    		tempIndexNOT = findIndexNOT(token);
	    		query = query + "NOT";
	    		if (tempIndexNOT + 2 == token.length){
	    			query = query + token[token.length - 1];
	    			System.out.println(query);
	    		}else{
		    		for(int i = tempIndexNOT + 1; i < token.length; i++){
		    			query = query + token[i];
		    		}
	    		}	

	    	}
	    	
	    	else{
	    		if ((tempIndexAND + 2) == token.length){
	    			query = query + token[token.length - 1];
	    		}else{
	    			query = query + "\"";
	    			for (int i = tempIndexAND + 1; i < token.length; i++){
	    				query = query + token[i];
	    			}
	    			query = query + "\"";
	    		}
	    	}
	   	}
	    
	    else{
	    	query = querystr;	
	    }	    
	    System.out.println(query);
	    
	    //2.2 parse query
	    Query q = new QueryParser("text", analyzer).parse(query);
	   
	    //3. search
	    int hitsPerPage = 10;
	    IndexReader reader = DirectoryReader.open(index);
	    IndexSearcher searcher = new IndexSearcher(reader);
	    TopScoreDocCollector collector = TopScoreDocCollector.create(hitsPerPage);
	    searcher.search(q, collector);
	    ScoreDoc[] hits = collector.topDocs().scoreDocs;
	    
	    //4.display results
	    System.out.println("Found " + hits.length + " hits.");
	    for (int i = 0; i < hits.length; i++){
	    	int docID = hits[i].doc;
	    	Document d = searcher.doc(docID);
	    	System.out.println((i + 1) + ". " + d.get("title") + " " + hits[i].score);
	    }
	    reader.close();    
	  }  
	  
	  //tools
	  private static void addDoc(IndexWriter w, String title, String text) throws IOException {
	    Document doc = new Document();
	    // use a string field for title because we don't want it tokenized
	    doc.add(new StringField("title", title, Field.Store.YES));
	    doc.add(new TextField("text", text, Field.Store.YES));
	    w.addDocument(doc);
	  }
	  
	  public static boolean hasAND(String[] token){
	  	for(int i = 0; i < token.length; i++){
	  		if (token[i].equals("AND")){
	  			return true;
	  		}
	  	}
	  	return false;
	  }
	  
	  public static int findIndexAND(String[] token){
	  	for(int i = 0; i < token.length; i++){
	  		if (token[i].equals("AND")){
	  			return i;
	  		}
	  	}
	  	return 0;
	  }
	  
	  public static boolean hasNOT(String token[]){
	  	for(int i = 0; i < token.length; i++){
	  		if (token[i].equals("NOT")){
	  			return true;
	  		}
	  	}
	  	return false;
	  }
	  
	  public static int findIndexNOT(String[] token){
	  	for(int i = 0; i < token.length; i++){
	  		if (token[i].equals("NOT")){
	  			return i;
	  		}
	  	}
	  	return 0;
	  }
}